var params = new URLSearchParams(window.location.search);

var nombre = params.get("nombre");
var sala = params.get("sala");

//referencias de jQuery

var divUsuarios = $("#divUsuarios");

var formEnviar = $("#formEnviar");

var txtMensaje = $("#txtMensaje");

var divChatbox = $("#divChatbox");

//funciones para renderizar usuarios

function renderizarUsuarios(personas) {
    var html ="";

    html += "<li>";
    html += 
        '<a href="javascript:void(0)" class="active"> Chat de <span>' +
        params.get("sala") +
        "</span></a>";
    html += "</li>";

    for (var i = 0; i < personas.length; i++){

        
        html += "<li>";
        html += 
            '<a data-id="' +
            personas[i].id +
            '"href="javascript:void(0)"><img src="assets/images/users/1.jpg" alt="user-img" class="img-circle"><span>' +
            personas[i].nombre + 
            '<small class="text-success">online</small></span></a>';
        html += "</li>";
    }

divUsuarios.html(html);
}

//renderizar mensajes

function renderizarMensajes(mensaje, yo) {
    var html ="";

    var fecha = new Date(mensaje.fecha);
    var hora = fecha.getHours() +":" + fecha.getMinutes();

    if(yo){
        html += '<li class="reverse">';
        html += '<div class="chat-content">';
        html += '<h5>'+mensaje.nombre+'</h5>';
        html += '<div class="box bg-light-inverse">'+mensaje.mensaje+'</div></div>';
        html += '<div class="chat-time">'+hora+'</div>';
        html += '<div class="chat-img"><img src="assets/images/users/5.jpg" alt="user" /></div>';
        html += '</li>';   
    } else {
        html += '<li>';
        html += '<div class="chat-img"><img src="assets/images/users/2.jpg" alt="user" /></div>';
        html += '<div class="chat-content">';
        html += '<h5>'+mensaje.nombre+'</h5>';
        html += '<div class="box bg-light-info">'+mensaje.mensaje+'</div></div>';
        html += '<div class="chat-time">'+hora+'</div>';
        html += '</li>';   
    }

    divChatbox.append(html);
}

//Listeners

divUsuarios.on("click", "a", function(){
    var id = $(this).data("id");

    console.log(id);
});

formEnviar.on("submit", function(e){
    e.preventDefault();

    if(txtMensaje.val().trim() === 0) {
        return;
    }

    socket.emit(
        "crearMensaje",
        {
            nombre: nombre,
            mensaje: txtMensaje.val(),
        },
        function(mensaje) {
            txtMensaje.val("").focus();
            renderizarMensajes(mensaje, true);
        }
    );
});


