const crearMensaje = (nombre, mensaje) => {

    var fecha = new Date();
    return {
        nombre,
        mensaje,
        fecha: new Date().getTime()
    }
}

module.exports = {
    crearMensaje
}