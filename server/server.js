const express = require('express');

const socketIO = require("socket.io");

const http = require("http");

const path = require('path');

const {Usuarios} =  require("./classes/usuarios");

const {crearMensaje} =  require("./utilidades/utilidades");


const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

//IO = esta en la comunicacion del backend
let io = socketIO(server);

const usuarios = new Usuarios();

io.on("connection", (client) => {
    console.log("Usuario conectado");

    
    
    client.on('entrarChat',(data, callback) => {
        if (!data.nombre || !data.sala){
            return callback ({
                error: true,
                mensaje: "El nombre/sala es encesario",
            });
        }

        //para unir a una sala
        client.join(data.sala);

        usuarios.agregarPersona(client.id, data.nombre, data.sala);

        //let personas = usuarios.agregarPersona(client.id, data.nombre);

        //client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        client.broadcast
            .to(data.sala)
            .emit("listaPersonas", usuarios.getPersonasPorSala(data.sala));

        callback(usuarios.getPersonasPorSala(data.sala));
        //callback(personas);
    }); 

    client.on('crearMensaje', (data, callback) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.to(persona.sala).emit("crearMensaje", mensaje);
        callback(mensaje);
    })

    client.on("disconnect", () => {
        let personaBorrada = usuarios.borrarPersona(client.id);


        client.broadcast.emit("crearMensaje", {
            usuario: "Administrador",
            mensaje: crearMensaje('Admin', `${personaBorrada.nombre} salio`),
        });

        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
    });

    client.on("mensajePrivado", (data) => {
        let persona = usuarios.getPersona(client.id);

        client.broadcast
            .to(data.para)
            .emit("mensajePrivado", crearMensaje(persona.nombre, data.mensaje));
    });
});

server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});